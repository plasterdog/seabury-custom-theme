<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'content', 'single' ); ?>

				<div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

			<?php
				// If comments are open or we have at least one comment, load up the comment template
				if ( comments_open() || '0' != get_comments_number() ) :
					comments_template();
				endif;
			?>

		<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>