<?php
/**
 * Template name: Affiliate parent
 *
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

<?php			// Get the logged-in user's affiliate id



$affiliate_id = affwp_get_affiliate_id();

// Get the parent affiliate of a given affiliate
$parent_affiliate = affwp_mlm_get_parent_affiliate( $affiliate_id );

// Get the direct referring affiliate of a given affiliate
$direct_affiliate = affwp_mlm_get_direct_affiliate( $affiliate_id );

?>
PARENT AFFILIATE: <?php echo $parent_affiliate ?><br/>
DIRECT AFFILIATE: <?php echo $direct_affiliate ?><br/>

<?php if( ! affwp_mlm_is_sub_affiliate( $affiliate_id ) ) {

echo 'this affiliate has no parent!';

}
?>


			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
