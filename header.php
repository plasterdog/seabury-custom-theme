<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package plasterdog
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!--[if lt IE 9]>
<script>
  var e = ("abbr,article,aside,audio,canvas,datalist,details," +
    "figure,footer,header,hgroup,mark,menu,meter,nav,output," +
    "progress,section,time,video,main").split(',');
  for (var i = 0; i < e.length; i++) {
    document.createElement(e[i]);
  }
</script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="full-top">
<div id="upper-band">
<div class="masthead-holder">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
<!-- inserts the header image-->
<div id="left-head">
	
	<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	<h2 class="masthead-info"><?php bloginfo( 'description' ); ?></h2>
</div><!-- end left head-->
<div id="right-head">

<?php dynamic_sidebar('top-right'); ?>
</div><!--ends right head -->			
		</div><!-- ends site branding -->
	</header><!-- ends masthead -->
</div><!-- ends masthead holder -->
<div class="clear"></div>
</div><!-- ends upper band-->
	</header><!-- #masthead -->

	<nav id="site-navigation" class="main-navigation" role="navigation">
		<!-- THIS IS WHERE YOU SET THE MENU TOGGLE TEXT -->
			<div class="masthead-holder">
			<h1 class="menu-toggle"><?php _e( 'toggle menu', 'plasterdog' ); ?></h1>
			<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'plasterdog' ); ?></a>

			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</div>
		</nav><!-- #site-navigation -->
		<div class="clear"></div>
</div> <!-- ends full top -->
		<div id="page" class="hfeed site">
	<div id="content" class="site-content">
