<?php
/**
 * @package plasterdog
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark">
<?php the_post_thumbnail( 'thumbnail' ); ?>
			<?php the_title(); ?></a></h1>

		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
        <p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the rest...</a></p>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
<?php the_excerpt(); ?>
<p align="right"><a href="<?php the_permalink(); ?>" rel="bookmark">... read the entire story</a></p>
	</div><!-- .entry-content -->

	<?php endif; ?>

	<footer class="entry-footer">
		

		<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
		<?php endif; ?>

		<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
<hr/>
