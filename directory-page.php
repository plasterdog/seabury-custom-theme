<?php
/*
*Template Name: Directory Page
 * @package plasterdog
 */

get_header(); ?>

<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
		</header><!-- .entry-header -->

	<div class="entry-content">
		<!--- SETTING UP THE QUERY FOR THE CUSTOM POST TYPE -->
				<?php	$args = array( 
							'post_type' => 'directory_item', 
							'posts_per_page' => -1,
							'order' => 'ASC',
							'meta_query' => array(
						        array(
						            'key' => 'sf_surname',
						            'value' => '',
						            'compare' => 'LIKE'
						        ),
						        array(
						            'key' => 'sf_surname_order',
						            'value' => '',
						            'compare' => 'LIKE'
						        ),
						        )
								 );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();?>
			
	<!--- SHOWING THE DIRECTORY ROW -->

			<ul>	
			<li><div class="clear"><h1><?php the_field('sf_surname'); ?></h1></div>

			<?php		// check for rows (parent repeater)
					if( have_rows('individual_family_member') ): ?>
			<?php   	// loop through rows (parent repeater)
					while( have_rows('individual_family_member') ): the_row(); ?>
	
	<!--- THE INDIVIDUAL FAMILY MEMBER LEFT SIDE -->	

					<div class="left-master"><?php the_sub_field('sf_first_name'); ?><br/>

					<!-- CONDITIONAL LANGUAGE USED TO KEEP THINGS COLLAPSED -->

					<?php if( get_sub_field('sf_primary_email_address') ): ?><?php the_sub_field('sf_primary_email_address'); ?><br/><?php endif; ?>
					<?php if( get_sub_field('sf_secondary_email_address') ): ?><?php the_sub_field('sf_secondary_email_address'); ?><?php endif; ?></div><!-- ends left master -->



						<?php // check for rows (sub repeater)
							if( have_rows('sf_residence_address') ): ?>
							<?php 
							// loop through rows (sub repeater)
								while( have_rows('sf_residence_address') ): the_row();
							// display each item as a list - with a class of completed ( if completed )
							?>	

	<!---THE INDIVIDUAL FAMILY MEMBER RIGHT SIDE (nested repeater field)-->
				
							<div class="right-master">

						<!-- CONDITIONAL LANGUAGE USED TO KEEP THINGS COLLAPSED -->
						<ul class="sf_address">
						<li>
						<?php if( get_sub_field('sf_mailing_address') ): ?><?php the_sub_field('sf_mailing_address'); ?><?php endif; ?>
						<?php if( get_sub_field('sf_phone_number') ): ?><?php the_sub_field('sf_phone_number'); ?><br/><?php endif; ?>
						<?php if( get_sub_field('sf_cell_phone_number') ): ?><?php the_sub_field('sf_cell_phone_number'); ?><?php endif; ?>
						</li>
						</ul>	
					</div><!-- ends right master -->

					<!-- CLOSING OUT THE NESTED REPEATING FEILD -->
							<?php endwhile; ?>			
							<?php endif; //if( get_sub_field('items') ): ?>
					
			<div class="clear"><hr/></div>
			<!--- CLOSING OUT THE MAIN REPEATER FIELD -->
					<?php endwhile; // while( has_sub_field('to-do_lists') ): ?> 	
				<?php endif; // if( get_field('to-do_lists') ): ?>

			</li>
			</ul>
<?php endwhile; // end of the primary loop. ?>

	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->
</main><!-- #main -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>

