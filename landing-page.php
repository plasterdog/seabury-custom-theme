<?php
/*
*Template Name: Landing Page
 * @package plasterdog
 */

get_header(); ?>

	<div id="primary" class="full-content-area">
		<main id="main" class="full-site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'plasterdog' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
	<hr/>
	<div class="landing-third-1">
	<?php the_field('first-third'); ?>
	</div><!-- ends landing third-->

	<div class="landing-third-2">
	<?php the_field('second-third'); ?>
	</div><!-- ends landing third-->

	<div class="landing-third-3">
		<?php dynamic_sidebar('landing-feed'); ?>
	</div><!-- ends landing third-->	
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
