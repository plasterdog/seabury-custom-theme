<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package plasterdog
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="foot-constraint">
			<div class="footer-section"><?php dynamic_sidebar('footer-1'); ?>	</div>
			<div class="footer-section"><?php dynamic_sidebar('footer-2'); ?>	</div>
			<div class="footer-section"><?php dynamic_sidebar('footer-3'); ?>	</div>
			<div class="site-info">
			<p>	&copy; <?php $the_year = date("Y"); echo $the_year; ?> | <?php bloginfo( 'name' ); ?> | All rights reserved<p></div><!-- .site-info -->
		</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-2699482-54', 'auto');
  ga('send', 'pageview');

</script>
		
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
